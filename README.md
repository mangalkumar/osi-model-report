# **OSI Model Layers**

## What is the OSI Model?
OSI stands for Open Systems Interconnection. OSI Model makes two or more devices connected and can communicate with each other even if both devices are connected to different networks.

## What are the OSI Model Layers?
There are seven layers of the OSI Model as shown in the picture 

![](https://www.cloudflare.com/img/learning/ddos/what-is-a-ddos-attack/osi-model-7-layers.svg)

## **We'll see each in detail one by one from top to bottom.**

## 7. The Application Layer
All network applications like Chrome, Firefox, or any other application that uses the network is dependent on this layer. Communication starts to network from this layer. This layer is composed of many protocols like HTTP, SMTP, FTP, Telnet, etc... These are the protocols that enable us to make communication over the network.

## 6. The Presentation Layer
This layer is mainly for making data ready for other layers so that other layers can use the data. The presentation layer is responsible for translation, encryption, and compression of data.
wo devices can be communicating using different methods, so layer 6 is responsible for converting incoming data into a good format.
ssume devices are communicating over an encrypted connection, layer 6 is responsible for adding the encryption on the sender’s end as well as decoding the encryption on the receiver's end so that it can present the application layer with unencrypted or say readable data.

## 5. The Session Layer
his layer is responsible for opening and closing communication between the two devices. Whenever two devices communicate a session is created. This session stays as long as communication continued.
he session layer also keep tracking data transfer with checkpoints. For example, if a 100-megabyte file is being transferred, the session layer could set a checkpoint every 5 megabytes. In the case of a disconnect or a crash after 52 megabytes have been transferred, the session could be resumed from the last checkpoint, so that downloading will not start from the beginning again.

## 4. The Transport Layer
Layer 4 is responsible for end-to-end communication between the two devices. This includes taking data from the session layer and breaking it up into small segments before sending it to layer 3. The transport layer on the receiving device is responsible for reassembling the segments into data the session layer can consume.
he transport layer is also responsible for flow control and error control. The transport layer performs error control on the receiving side by ensuring that the data received is complete and making another request if it is not complete.
![](https://www.cloudflare.com/img/learning/ddos/glossary/open-systems-interconnection-model-osi/4-transport-layer.svg)

## 3. The Network Layer
he network layer is responsible for making data transfer between different networks. The network layer breaks up segments from the transport layer into smaller units, called packets. The network layer also searches for the best path for the data to reach its destination.
![](https://www.cloudflare.com/img/learning/ddos/glossary/open-systems-interconnection-model-osi/2-data-link-layer.svg)

## 2. The Data Link Layer
The data link layer is very similar to the network layer. The data that comes from the network layer, ti converts data into smaller pieces which are also called frames. The data link layer is responsible for flow control of data and error control during communication.

## 1.  The Physical Layer
This layer includes all physical devices like cables and switches to establish a connection on a network. The data is converted into a bitstream, which is a string of 1s and 0s. This ensures that the devices have the same signal convention so that 1s can be distinguished from 0s.
---
# Bibliography
1. [ OSI Model Blog Post](https://www.cloudflare.com/en-gb/learning/ddos/glossary/open-systems-interconnection-model-osi/)

2. [OSI Model Video explanation](https://www.youtube.com/watch?v=vv4y_uOneC0)